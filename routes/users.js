const express = require('express');
const { users } = require('../models');

const router = express.Router();

async function createUser(req, res) {
  // TODO: body validation

  // add the body to the model
  const id = await users.add(req.body);

  return (id)
    ? res.status(200).send(id)
    : res.status(409).send('Email Already Taken');
}

async function listUsers(req, res) {
  const ids = await users.find(
    req.headers['search-email'],
    req.headers['search-fullname'],
  );
  res.status(200).send(ids);
}

async function getUserById(req, res) {
  // TODO: id validation

  const user = await users.getById(req.params.id);

  return user
    ? res.status(200).send(user)
    : res.status(404).send('Not Found');
}

async function deleteUserById(req, res) {
  // TODO: id validation
  const ret = await users.deleteById(req.params.id);

  return ret
    ? res.status(200).send('OK')
    : res.status(404).send('Not Found');
}

async function updateUserById(req, res) {
  // TODO: id validation
  // TODO: ensure the email is unique before updating
  const ret = await users.updateById(req.params.id, req.body);

  return ret
    ? res.status(200).send('OK')
    : res.status(404).send('Not Found');
}

// setup express routes with
router
  .route('/')
  .post((req, res) => createUser(req, res))
  .get((req, res) => listUsers(req, res));

router
  .route('/:id')
  .get((req, res) => getUserById(req, res))
  .put((req, res) => updateUserById(req, res))
  .delete((req, res) => deleteUserById(req, res));

module.exports = router;
