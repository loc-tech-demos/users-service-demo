const express = require('express');

const jwtHelper = require('../lib/jwtHelper');
const { users } = require('../models');

const router = express.Router();

async function getSelf(req, res) {
  // extract email from JWT
  // not doing any JWT validation
  // we assume it has passed through some authorizer outside of this scope
  const email = await jwtHelper.getEmailFromHeader(req.headers);
  if (!email) {
    return res.status(401).send('Unauthorized');
  }

  // get the user via email
  const user = await users.getByEmail(email);

  return user
    ? res.status(200).send(user)
    : res.status(404).send('Not Found');
}

async function updateSelf(req, res) {
  // extract email from JWT
  // not doing any JWT validation
  // we assume it has passed through some authorizer outside of this scope
  const email = await jwtHelper.getEmailFromHeader(req.headers);
  if (!email) {
    return res.status(401).send('Unauthorized');
  }

  // TODO: ensure the email is unique before updating

  const ret = await users.updateByEmail(email, req.body);

  return ret
    ? res.status(200).send('OK')
    : res.status(404).send('Not Found');
}

// setup express routes with
router
  .route('/')
  .put((req, res) => updateSelf(req, res))
  .get((req, res) => getSelf(req, res));

module.exports = router;
