module.exports = {
  setupFiles: ['<rootDir>/__tests__/env.js'],
  collectCoverage: true,
  testMatch: [
    '**/__tests__/*.test.js',
  ],
  moduleNameMapper: {
    // Force module uuid to resolve with the CJS entry point, because Jest does not support package.json.exports. See https://github.com/uuidjs/uuid/issues/451
    uuid: require.resolve('uuid'),
  },
  preset: '@shelf/jest-mongodb',
};
