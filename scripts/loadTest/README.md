# Load Testing

This is a set of [Artillery](https://www.artillery.io) scripts to perform load testing on this Users service demo.

## How to run

### Prerequisites

- You have a Users service demo running somewhere. Note the URL.
- You have understanding of Artillery, and you know what the `config.yml` does to set up the test.
- You have a dataset loaded in your database
  - See this [Script to create random data](https://gitlab.com/loc-tech-demos/users-service-demo/-/issues/22) issue for how to create random data
- With that dataset, you have a `users.csv` file that describes your data
  - `users.csv` gets loaded into the test, so that the test has user metadata to get
  - You want to trim `users.csv` to have maybe 20-30 users, so it loads fast
  - You need to get rid of the csv file header
- If you're not testing `localhost`, you have auth infrastructure set up
  - See this [Script to get tokens](https://gitlab.com/loc-tech-demos/users-service-demo/-/issues/11) issue for how to generate a token

### Commands

```sh
# runs the `user-read-change.yml` scenario with the basic `config.yml`
npx artillery run --config config.yml scenarios/user-read-change.yml --output report.json

# with the `report.json` from above, create a .html report
artillery report report.json
```

```sh
# same as above, except we specify another target
# this is useful when switching between different envs
# note that this depends on a Admin TOKEN variable set. check the `getToken` script.
npx artillery run --config config.yml scenarios/user-read-change.yml -t https://api.users.demo.locborgtus.net/v1 -v '{"token": "${TOKEN}"}' --output report.json

# with the `report.json` from above, create a .html report
artillery report report.json
```
