/* eslint-disable no-console */
require('dotenv').config();
const axios = require('axios');
const async = require('async');
const casual = require('casual');
const { Parser } = require('json2csv');
const fs = require('fs');
const http = require('http');
const https = require('https');

const list = [];
let count = 0;
const statusFreq = 100;

// configuring axios to use keepalive
// otherwise, we can run out of source network ports if our ops are too fast
const httpAgent = new http.Agent({ keepAlive: true });
const httpsAgent = new https.Agent({ keepAlive: true });

function generatePayload() {
  // TODO: get schema from OpenAPI

  // the default email randomness isn't enough, so we're creating more randomness here
  // this adds a number after the email name, but retains the domain
  const [name, domain] = casual.email.split('@');
  const randEmail = `${name}${casual.integer()}@${domain}`;
  const p = {
    fullName: casual.full_name,
    email: randEmail,
  };

  return p;
}

async function create() {
  const payload = generatePayload();

  // check if an error occurred
  // an acceptable error could be duplicate email, since our random email generator
  // doesn't guarantee an unique email
  // if so, simply bypass the error. this is acceptable.

  try {
    const response = await axios.post(process.env.endpoint, payload, {
      headers: { Authorization: `Bearer ${process.env.token}` },
      httpAgent,
      httpsAgent,
    });
    payload.id = response.data;

    // stores the entry and count, because the axios post was successful
    list.push(payload);
    count += 1;

    // prints status over time
    if (count % statusFreq === 0) {
      console.log(`Added ${count} users`);
    }
  } catch (err) {
    if (err.response && err.response.status === 409) {
      console.log('Duplicate email, skipping');
    } else {
      // we have a bigger problem
      console.log(err);
      throw err;
    }
  }
}

function saveCSV() {
  // const parser = new Parser(opts);
  const parser = new Parser();
  const csv = parser.parse(list);
  fs.writeFileSync('data.csv', csv);
}

async function main() {
  // create an array that stores the function, so we can parallelize
  const num = parseInt(process.env.num, 10);
  const arr = Array(num).fill(create);
  const p = process.env.parallelLimit;

  // issue a parallel with a limit, so that this can be done quick
  async.parallelLimit(arr, p, (err) => {
    if (err) {
      console.error(err);
    } else {
      saveCSV();
      console.log(`Generated ${count} users`);
    }
  });
}

main();
