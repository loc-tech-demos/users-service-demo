# Create Random Users

This is a script that creates random users.

This script does the following:

- Loads settings from the .env file
- creates an array that holds functions to create users
  - the create functions uses the `casual` package to generate data
- use the `async` package to parallelize the user creation
- saves the user list to a CSV file called `data.csv`

## How to run

1. Copy the `env.sample` file to `.env`
2. Configure the .env file
3. Make sure the Users service is running
4. Run the script with `node .`

## Dump and Restore Mongo data

Once you've ran this script, you have a populated Mongo database. You can save this database, so that you can load the dataset for repeatability afterwards.

To save, do something like:

```sh
mongodump --uri mongodb+srv://<username>:<password>@cluster0.suhci.mongodb.net/users-service-demo --collection users
# this puts the dump data in a folder structure like:
# ./dump/<database>/<collection>.bson
```

To restore, do:

```sh
# make sure to specify the dump file
mongorestore --uri mongodb+srv://<username>:<password>@cluster0.suhci.mongodb.net/users-service-demo --collection users dump/users-service-demo/users.bson
```