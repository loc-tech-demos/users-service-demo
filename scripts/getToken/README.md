# GetToken script

This is a convenience script to get a JWT Access Token.

This way, we don't need a demo frontend where we have to do a Universal Login, and extract JWT out of the browser console.

## How

1. Copy `env.sample` to `env`
2. Configure `env`
3. Run `./getToken.sh`.
  - Or optionally, if you want a specific `env` file, run something like `./getToken.sh env.user`

## Configuring the env file

```env
DOMAIN=<no https, just the url to your Auth0 tenant. Ends in auth0.com>
CLIENT_ID=<the application ID in Auth0>
USER=<your user name, has to exist in Auth0 user db>
PASSWORD=<password. escape it in '' if it has weird characters>
AUDIENCE=<the audience for your Auth0 API>
CLAIMS=<url encoded string, space separated scopes. Like openid%20email%20profile%20user>
```
