#!/bin/sh

# Prerequisites
# Configure the env file. Be sure to urlencode parameters as needed
# Auth0 must be configured with
# - Settings -> Tenant Settings -> Default Directory = Username-Password-Authentication
# - the user already exists in the database
# - the Application -> Advanced Settings -> Grant Types -> Password = enabled
# Understand that this is not as secure as the other flows. For dev/test purposes, this works easily.

ENV="${1:-env}"

export $(grep -v '^#' ${ENV} | xargs)

curl --request POST -s \
  --url https://${DOMAIN}/oauth/token \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data grant_type=password \
  --data username=${USER} \
  --data password=${PASSWORD} \
  --data audience=${AUDIENCE} \
  --data scope="${CLAIMS}" \
  --data client_id=${CLIENT_ID} \
  | jq -r .access_token
