# Auth0 post-login Action

This Auth0 action adds an email address to the JWT. This way, our application knows the email address of the user. We can use this email address for business logic.

TECHNICALLY, you don't have to have this Action. A mitigation is for the backend service to retrieve the email through the OAuth 2 `/profile` endpoint. If the user has an `email` claim, this can also retrieve the email. However, this does incur an extra round trip to the OAuth service. So, this Action saves that trip. But, as a fallback, the service should also get the email from `/profile`.

## How

1. Go to Auth0
2. Go to Actions
3. Click `Library`
4. Click `Build Custom`
5. Give it a name, like `Add Email after Login`
6. Leave Trigger as `Login / Post Login`, Runtime to be `Node 16`
7. Paste in the code in the `index.js` file here
8. Save the Action
9. Go to `Flows`
10. Click `Login`
11. Click `Custom` in the right
12. Drag the Action you just built between the Start and Complete cirlces
13. Click `Apply`

Now, the JWT should contain a key such as:

```
"https://api.users.demo.locborgtus.net/email": "loc@example.com"
```
