/* eslint-disable camelcase */
const axios = require('axios');
const auth0 = require('auth0');

const endpoint = 'https://api.users.demo.locborgtus.net/v1/users';
const domain = 'dev-fgda3k69.us.auth0.com';
// make sure this is a M2M application ID
const client_id = 'Lr0y0XiIMoEqAk4Qx0cpBoOdLo1X7sc5';
const audience = 'api.users.demo.locborgtus.net';
const default_role = 'rol_yw2GI9CH8ZusE1Nv';

/**
 * Adds a default role to this new user.
 *
 * @param {Event} event - Details about the context and user that has registered.
 */
const addRole = async (event) => {
  const { ManagementClient } = auth0;

  const management = new ManagementClient({
    domain,
    clientId: client_id,
    clientSecret: event.secrets.client_secret,
  });

  const params = { id: event.user.user_id };
  const data = { roles: [default_role] };

  await management.assignRolestoUser(params, data);
};

/**
 * Adds a user to our system of record.
 *
 * @param {Event} event - Details about the context and user that has registered.
 */
const addUser = async (event) => {
  // get an auth token for our endpoint
  const authPayload = {
    client_id,
    client_secret: event.secrets.client_secret,
    audience,
    grant_type: 'client_credentials',
  };

  const authResponse = await axios.post(`https://${domain}/oauth/token`, authPayload);

  // add this new user to our system of record
  const config = {
    headers: { Authorization: `Bearer ${authResponse.data.access_token}` },
  };

  const payload = {
    email: event.user.email,
    fullName: `${event.user.given_name} ${event.user.family_name}`,
    auth0: {
      user_id: event.user.user_id,
    },
  };

  await axios.post(endpoint, payload, config);
};

/**
* Handler that will be called during the execution of a PostUserRegistration flow.
*
* @param {Event} event - Details about the context and user that has registered.
*/
exports.onExecutePostUserRegistration = async (event) => {
  try {
    // assign a default role for this new user
    await addRole(event);

    // add a user in our system of record
    await addUser(event);

    return;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }
};
