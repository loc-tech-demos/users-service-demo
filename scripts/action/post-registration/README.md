# Auth0 post-registration Action

This Auth0 action calls an endpoint (POST) with a payload. This way, our backend knows when a new Auth0 user is registered. We want to sync up Auth0 and our own System of Record.

## How

1. Go to Auth0
2. Go to Actions
3. Click `Library`
4. Click `Build Custom`
5. Give it a name, like `New User`
6. Choose trigger as `Post User Registration`, Runtime to be `Node 16`
7. Paste in the code in the `index.js` file here
8. Add your secrets to the `Secrets` Menu to the left
9. Save the Action
10. Go to `Flows`
11. Click `Post User Registration`
12. Click `Custom` in the right
13. Drag the Action you just built between the Start and Complete cirlces
14. Click `Apply`
