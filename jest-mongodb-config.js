module.exports = {
  mongodbMemoryServerOptions: {
    instance: {
      dbName: 'jest',
      port: 27018,
    },
    binary: {
      version: '5.0.6', // Version of MongoDB
      skipMD5: true,
    },
    autoStart: false,
  },
};
