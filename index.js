const express = require('express');
const models = require('./models');

const app = express();
app.use(express.json());

const port = process.env.PORT;

app.use('/v1/users', require('./routes/users'));
app.use('/v1/user', require('./routes/user'));

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  // format error
  res.status(err.status || 500).json({
    message: err.message,
    errors: err.errors,
  });
});

async function main() {
  await models.init();
  // eslint-disable-next-line no-console
  app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
}

main();

process.on('SIGINT', () => {
  models.close();
  process.exit();
});
