process.env.MONGODB_URI = 'mongodb://localhost:27018';
process.env.MONGODB_COLLECTION = 'users';
process.env.PORT = 8081;
process.env.AUDIENCE = 'https://audience.example.com';
process.env.ISSUER_BASE_URL = 'http://example.com';
