const axios = require('axios');
const express = require('express');
const jwt = require('jsonwebtoken');

const models = require('../models');

jest.mock('auth0');

// test file for the user self routes

const port = process.env.PORT;
const url = `http://localhost:${port}/v1`;

const app = express();
app.use(express.json());

app.use('/v1/users', require('../routes/users'));
app.use('/v1/user', require('../routes/user'));

let server = null;

const apiSpec = require('../openapi/users.json');

const example = apiSpec.components.schemas.User['x-examples']['Basic User'];

const newName = 'Nelson Muntz';

beforeAll(async () => {
  const testName = __filename.slice(__dirname.length + 1);
  process.env.MONGODB_COLLECTION += `-${testName}`;
  await models.init();

  // eslint-disable-next-line no-console
  server = app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
});

describe('setup', () => {
  let ids = [];

  it('should list users with no item', async () => {
    const response = await axios.get(`${url}/users`);
    ids = response.data;
    expect(ids.length).toBe(0);
  });

  it('should create an user', async () => {
    const response = await axios.post(`${url}/users`, example);
    expect(response.status).toEqual(200);
    expect(response.statusText).toEqual('OK');
  });
});

describe('basic routes', () => {
  let jwtWithEmail = '';
  let jwtWithNoEmail = '';

  it('should create JWTs', async () => {
    // empty payload is fine - we're only using the email key
    const payload = {};

    jwtWithNoEmail = jwt.sign(payload, 'some key');

    payload[`${process.env.AUDIENCE}/email`] = example.email;
    jwtWithEmail = jwt.sign(payload, 'some key');
  });

  it('should get myself', async () => {
    const config = {
      headers: { Authorization: `Bearer ${jwtWithEmail}` },
    };

    const response = await axios.get(`${url}/user`, config);

    const { data } = response;
    expect(data.fullName).toEqual(example.fullName);
    expect(data.email).toEqual(example.email);
  });

  it('should mutate myself', async () => {
    const config = {
      headers: { Authorization: `Bearer ${jwtWithEmail}` },
    };

    // get the data for some template
    let response = await axios.get(`${url}/user`, config);
    const { data } = response;

    // mutate
    data.fullName = newName;
    example.fullName = newName;

    // update
    response = await axios.put(`${url}/user`, data, config);

    // get self again
    response = await axios.get(`${url}/user`, config);

    expect(data.fullName).toEqual(example.fullName);
    expect(data.email).toEqual(example.email);
  });

  it('should get myself, with call to /profile', async () => {
    const config = {
      headers: { Authorization: `Bearer ${jwtWithNoEmail}` },
    };

    const response = await axios.get(`${url}/user`, config);

    const { data } = response;
    expect(data.fullName).toEqual(example.fullName);
    expect(data.email).toEqual(example.email);
  });

  it('should fail if not given a JWT', async () => {
    try {
      await axios.get(`${url}/user`);
    } catch (err) {
      expect(err.response.status).toEqual(401);
    }
  });

  it('should fail if call to /profile has no email', async () => {
    try {
      await axios.get(`${url}/user`);
    } catch (err) {
      expect(err.response.status).toEqual(401);
    }
  });
});

afterAll(async () => {
  await models.close();
  server.close();
});
