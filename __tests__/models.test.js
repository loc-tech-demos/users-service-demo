/* eslint-disable dot-notation */
const uuid = require('uuid');

const models = require('../models');
const apiSpec = require('../openapi/users.json');

const example = apiSpec.components.schemas.User['x-examples']['Basic User'];

beforeAll(async () => {
  const testName = __filename.slice(__dirname.length + 1);
  process.env.MONGODB_COLLECTION += `-${testName}`;
  await models.init();
});

describe('ID flow', () => {
  let id = '';

  it('should add an user', async () => {
    id = await models.users.add(example);
    expect(uuid.validate(id)).toBeTruthy();
  });

  it('should have the added user in the user ID list', async () => {
    const ids = await models.users.find();
    expect(ids.includes(id)).toBeTruthy();
  });

  it('should have saved the user', async () => {
    const data = await models.users.getById(id);
    expect(data.fullName).toEqual(example.fullName);
    expect(data.email).toEqual(example.email);
  });

  it('should update by ID', async () => {
    const fullName = 'Nelson Muntz';
    await models.users.updateById(id, { fullName });
    const data = await models.users.getById(id);
    expect(data.fullName).toEqual(fullName);
  });

  it('should check for duplicates while adding', async () => {
    const res = await models.users.add(example);
    expect(res).toBeFalsy();
  });

  it('should delete by ID', async () => {
    await models.users.deleteById(id);
    const data = await models.users.getById(id);
    expect(data).toBeFalsy();
  });
});

describe('email flow', () => {
  let id = '';

  it('should add an user', async () => {
    id = await models.users.add(example);
    expect(uuid.validate(id)).toBeTruthy();
  });

  it('should get a user by email', async () => {
    const user = await models.users.getByEmail(example.email);
    expect(user.fullName).toEqual(example.fullName);
    expect(user.email).toEqual(example.email);
  });

  it('should update by email', async () => {
    const newDoc = {
      email: 'foobar@example.com',
      fullName: 'Nelson Muntz',
    };

    await models.users.updateByEmail(example.email, newDoc);
    const data = await models.users.getById(id);
    expect(data.fullName).toEqual(newDoc.fullName);
    expect(data.email).toEqual(newDoc.email);
  });
});

afterAll(async () => {
  await models.close();
});
