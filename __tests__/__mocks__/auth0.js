const apiSpec = require('../../openapi/users.json');

const example = apiSpec.components.schemas.User['x-examples']['Basic User'];

class AuthenticationClient {
  constructor() {
    // eslint-disable-next-line no-extra-semi
    ;
  }

  // eslint-disable-next-line class-methods-use-this
  getProfile() {
    const payload = {
      email: example.email,
    };
    return payload;
  }
}

const auth0 = {
  AuthenticationClient,
};

module.exports = auth0;
