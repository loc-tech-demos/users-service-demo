const axios = require('axios');
const express = require('express');
const { v4: uuidv4 } = require('uuid');

const models = require('../models');

// test file for the admin routes

const port = process.env.PORT;
const url = `http://localhost:${port}/v1`;

const app = express();
app.use(express.json());

app.use('/v1/users', require('../routes/users'));

let server = null;

const apiSpec = require('../openapi/users.json');

const example = apiSpec.components.schemas.User['x-examples']['Basic User'];

beforeAll(async () => {
  const testName = __filename.slice(__dirname.length + 1);
  process.env.MONGODB_COLLECTION += `-${testName}`;
  await models.init();

  // eslint-disable-next-line no-console
  server = app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
});

describe('basic routes', () => {
  let ids = [];
  it('should list users with no item', async () => {
    const response = await axios.get(`${url}/users`);
    ids = response.data;
    expect(ids.length).toBe(0);
  });

  it('should create an user', async () => {
    const response = await axios.post(`${url}/users`, example);
    expect(response.status).toEqual(200);
    expect(response.statusText).toEqual('OK');
  });

  it('should list users with 1 item', async () => {
    const response = await axios.get(`${url}/users`);
    ids = response.data;
    expect(ids).toHaveLength(1);
  });

  it('should get the user that was added', async () => {
    const response = await axios.get(`${url}/users/${ids[0]}`);
    const { data } = response;

    // clean up the data that the endpoint will mutate
    expect(data.fullName).toEqual(example.fullName);
    expect(data.email).toEqual(example.email);
  });

  it('should return nothing if user ID is not found', async () => {
    try {
      await axios.get(`${url}/users/${uuidv4()}`);
    } catch (err) {
      expect(err.response.status).toEqual(404);
      expect(err.response.data).toEqual('Not Found');
    }
  });

  it('should update the user', async () => {
    const newName = 'Nelson Muntz';

    // get the user for some template data
    let response = await axios.get(`${url}/users/${ids[0]}`);
    const { data } = response;

    // mutate
    data.fullName = newName;

    // update
    response = await axios.put(`${url}/users/${ids[0]}`, data);

    // get the user again
    response = await axios.get(`${url}/users/${ids[0]}`);

    expect(response.data.fullName).toEqual(newName);
  });

  it('should be unable to update an unknown user id', async () => {
    try {
      await axios.put(`${url}/users/${uuidv4()}`, {});
    } catch (err) {
      expect(err.response.status).toEqual(404);
    }
  });

  it('should be unable to add a duplicate user with the same email', async () => {
    try {
      await axios.post(`${url}/users`, example);
    } catch (err) {
      expect(err.response.status).toEqual(409);
    }
  });

  it('should be unable to delete an unknown user id', async () => {
    try {
      await axios.delete(`${url}/users/${uuidv4()}`);
    } catch (err) {
      expect(err.response.status).toEqual(404);
    }
  });

  it('should delete the user', async () => {
    let response = await axios.delete(`${url}/users/${ids[0]}`);
    expect(response.status).toEqual(200);

    response = await axios.get(`${url}/users`);
    expect(response.data).toHaveLength(0);
  });
});

afterAll(async () => {
  await models.close();
  server.close();
});
