const jwtDecode = require('jwt-decode');
const isemail = require('isemail');
const { AuthenticationClient } = require('auth0');

// initialize the auth0 client, so we can get more client identity like email
const auth0 = new AuthenticationClient({
  domain: new URL(process.env.ISSUER_BASE_URL).hostname,
  clientId: process.env.CLIENT_ID,
});

function getBearerToken(headers) {
  if (!headers || !headers.authorization || !headers.authorization.startsWith('Bearer ')) {
    return null;
  }

  return headers.authorization.substring('Bearer '.length);
}

async function getEmail(jwt) {
  const token = jwtDecode(jwt);

  // check if the email is part of the token
  // should be in key called '${audience}/email'
  // this assumes an Auth0 action was setup to add this field
  const email = token[`${process.env.AUDIENCE}/email`];
  if (email && isemail.validate(email)) return email;

  // if the email doesn't exist, or is invalid, get the email with a call to /userinfo
  try {
    const userinfo = await auth0.getProfile(jwt);
    return userinfo.email;
  } catch (err) {
    return null;
  }
}

async function getEmailFromHeader(headers) {
  // process headers
  const bearerToken = getBearerToken(headers);
  if (!bearerToken) {
    return null;
  }

  // get the email
  const email = await getEmail(bearerToken);
  return email;
}

module.exports = {
  getBearerToken,
  getEmail,
  getEmailFromHeader,
};
