# Users Service Demo

Working demo of a Users service. The goals are to de-risk how long it takes to create a REST Service:

1. The REST service are well-defined
2. The REST service is well-protected
3. The REST service is integrated with an Identity service (Auth0)
4. The REST service is deployed into an infrastructure
5. The REST service is simple and easy to develop

## Getting started

This is a typical Node.js application. To run, do:

```sh
git clone <the repo url>
npm ci
npm start
```

To test, do:

```sh
npm run test
```

To develop, do:

```sh
npx nodemon node .
# and edit away at your code
```

You might have to install other system dependencies, but that's up to you to figure out.

## Docker image HOWTO

In general, the steps are:

1. Build the image
2. Push the image to your registry

Example:

```sh
# assumes: docker is available, aws-cli is configured, an ECR repo is made

# build the image
npm run image:build

# get your ECR passowrd, and tell docker to login to ECR
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 183147974733.dkr.ecr.us-west-2.amazonaws.com/users-service-demo

# tag your local image to the ECR image
docker tag users-service-demo:latest 183147974733.dkr.ecr.us-west-2.amazonaws.com/users-service-demo:latest

# push your image to ECR
docker push 183147974733.dkr.ecr.us-west-2.amazonaws.com/users-service-demo:latest

```
