{
  "openapi": "3.0.3",
  "x-stoplight": {
    "id": "ruw29qwiaqmaj"
  },
  "info": {
    "title": "Users",
    "version": "1.0",
    "contact": {
      "name": "Simon Wong",
      "email": "locborgtus@gmail.com",
      "url": "http://www.locborgtus.net"
    },
    "description": "Users service demo"
  },
  "servers": [
    {
      "url": "http://localhost:8080",
      "description": "Local Development"
    },
    {
      "url": "https://api.users.demo.locborgtus.net",
      "description": "Production"
    }
  ],
  "paths": {
    "/v1/user": {
      "parameters": [],
      "get": {
        "summary": "Get self User",
        "operationId": "get-v1-user",
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/User"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "tags": [
          "read-methods",
          "user"
        ],
        "description": "Get self User",
        "security": [
          {
            "Auth0": [
              "user"
            ]
          }
        ]
      },
      "put": {
        "summary": "Update Self User",
        "operationId": "put-v1-user",
        "description": "Update self user",
        "tags": [
          "write-methods",
          "user"
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "Auth0": [
              "user"
            ]
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/User"
              }
            }
          }
        }
      }
    },
    "/v1/users/{id}": {
      "parameters": [
        {
          "schema": {
            "type": "string",
            "format": "uuid",
            "example": "23249CA6-8ACD-4988-A2D7-AF80FBD0678A"
          },
          "name": "id",
          "in": "path",
          "required": true,
          "description": "User ID"
        }
      ],
      "get": {
        "summary": "Get a User",
        "operationId": "get-v1-user-id",
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/User"
                },
                "examples": {}
              }
            }
          },
          "404": {
            "description": "Not Found"
          }
        },
        "description": "Get a user by their ID",
        "tags": [
          "read-methods",
          "admin"
        ],
        "security": [
          {
            "Auth0": [
              "admin:read"
            ]
          }
        ]
      },
      "put": {
        "summary": "Update a User",
        "operationId": "put-v1-user-id",
        "responses": {
          "200": {
            "description": "OK"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "404": {
            "description": "Not Found"
          }
        },
        "description": "Update a User by their ID",
        "tags": [
          "admin",
          "write-methods"
        ],
        "x-internal": false,
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/User"
              }
            }
          }
        },
        "security": [
          {
            "Auth0": [
              "admin:write"
            ]
          }
        ]
      },
      "delete": {
        "summary": "Delete a User",
        "operationId": "delete-v1-user-id",
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "Not Found"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "description": "Delete a User by their ID",
        "tags": [
          "write-methods",
          "admin"
        ],
        "x-internal": false,
        "security": [
          {
            "Auth0": [
              "admin:write"
            ]
          }
        ]
      }
    },
    "/v1/users": {
      "get": {
        "summary": "Get User ID List",
        "tags": [
          "read-methods",
          "admin"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string",
                    "format": "uuid",
                    "readOnly": true
                  }
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "operationId": "get-v1-users",
        "description": "Get a list of user IDs",
        "parameters": [
          {
            "schema": {
              "type": "string",
              "format": "email"
            },
            "in": "header",
            "name": "search-email",
            "description": "User email address"
          },
          {
            "schema": {
              "type": "string"
            },
            "in": "header",
            "name": "search-fullName",
            "description": "User full name"
          }
        ],
        "security": [
          {
            "Auth0": [
              "admin:read"
            ]
          }
        ]
      },
      "post": {
        "summary": "Create a User",
        "operationId": "post-v1-users",
        "responses": {
          "200": {
            "$ref": "#/components/responses/UUID"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "409": {
            "description": "Email Already Taken"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "description": "Create a new user",
        "tags": [
          "admin",
          "write-methods"
        ],
        "security": [
          {
            "Auth0": [
              "admin:write"
            ]
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/User"
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "User": {
        "title": "User",
        "type": "object",
        "description": "",
        "x-examples": {
          "Basic User": {
            "id": "497f6eca-6276-4993-bfeb-53cbbbba6f08",
            "fullName": "Jimbo Jones",
            "email": "user@example.com",
            "createDate": "2019-08-24T14:15:22Z"
          }
        },
        "properties": {
          "id": {
            "type": "string",
            "description": "Unique identifier for the given user.",
            "format": "uuid",
            "readOnly": true
          },
          "fullName": {
            "type": "string",
            "example": "Jimbo Jones",
            "pattern": "^[A-Za-z0-9-_\\. ]+"
          },
          "email": {
            "type": "string",
            "format": "email",
            "example": "user@example.com"
          },
          "createDate": {
            "type": "string",
            "format": "date-time",
            "description": "The date that the user was created.",
            "readOnly": true
          }
        },
        "required": [
          "fullName",
          "email"
        ]
      }
    },
    "securitySchemes": {
      "Auth0": {
        "type": "oauth2",
        "flows": {
          "authorizationCode": {
            "tokenUrl": "https://dev-fgda3k69.us.auth0.com/oauth/token",
            "scopes": {
              "user": "User self access",
              "admin:read": "Read access to admin functions",
              "admin:write": "Write access to admin functions"
            },
            "authorizationUrl": "https://dev-fgda3k69.us.auth0.com/authorize"
          }
        }
      }
    },
    "responses": {
      "UUID": {
        "description": "Example response",
        "content": {
          "application/json": {
            "schema": {
              "type": "string",
              "format": "uuid",
              "example": "23249CA6-8ACD-4988-A2D7-AF80FBD0678A"
            }
          }
        }
      }
    },
    "examples": {},
    "requestBodies": {}
  },
  "tags": [
    {
      "name": "admin"
    },
    {
      "name": "read-methods"
    },
    {
      "name": "user"
    },
    {
      "name": "write-methods"
    }
  ]
}