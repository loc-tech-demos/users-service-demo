const users = require('./users');

const models = {
  users,
  close() {
    users.close();
  },
  async init() {
    try {
      await users.init();
      // eslint-disable-next-line no-console
      console.log('models initialized');
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error('models init failed');
      throw err;
    }
  },
};

module.exports = models;
