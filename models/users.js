/* eslint-disable dot-notation */
require('dotenv').config();
const { MongoClient } = require('mongodb');
const { v4: uuidv4 } = require('uuid');
const cloneDeep = require('clone-deep');

let coll = null;

// initialize the mongo client
const client = new MongoClient(
  `${process.env.MONGODB_URI}?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true },
);

async function init() {
  // connect to database
  await client.connect();
  coll = client.db().collection(process.env.MONGODB_COLLECTION);
}

async function getByEmail(email) {
  return coll.findOne({ email });
}

async function add(data) {
  const d = cloneDeep(data);
  if (d['_id']) delete d['_id'];

  // check if duplicate exists
  // currently keying on email being unique
  if (await getByEmail(data.email)) {
    return null;
  }

  // fill in the blanks
  d.createDate = `${new Date().toISOString()}`;
  d.id = uuidv4();

  // insert to db
  await coll.insertOne(d);

  return d.id;
}

async function find(email, fullName) {
  // form query
  const query = {};
  if (email) query.email = email;
  if (fullName) query.fullName = fullName;

  // form projection
  const proj = { id: 1, _id: 0 };

  // query - watch for the await, which returns a promise for .map
  return (await coll
    .find(query)
    .project(proj)
    .toArray())
    .map((m) => m.id);
}

async function getById(id) {
  return coll.findOne({ id });
}

async function deleteById(id) {
  const res = await coll.deleteOne({ id });
  return (!!res.deletedCount);
}

async function updateById(id, data) {
  const d = cloneDeep(data);
  if (d['_id']) delete d['_id'];

  const res = await coll.updateOne({ id }, [{ $set: d }]);
  return (!!res.matchedCount);
}

async function updateByEmail(email, data) {
  const d = cloneDeep(data);
  if (d['_id']) delete d['_id'];

  const res = await coll.updateOne({ email }, [{ $set: d }]);
  return (!!res.matchedCount);
}

function close() {
  client.close();
}

const model = {
  add,
  find,
  getById,
  deleteById,
  updateById,
  getByEmail,
  updateByEmail,
  init,
  close,
};

module.exports = model;
